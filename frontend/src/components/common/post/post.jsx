import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';

import { useSelector } from 'react-redux';
import { image as imageService } from 'src/services/services';
import styles from './styles.module.scss';
import EditPost from '../../thread/components/edit-post/edit-post';

const Post = (
  { post, onPostLike, onPostDislike, onExpandedPostToggle, sharePost, handlePostEdit, onPostDelete }
) => {
  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostDelete = () => onPostDelete(id);

  const [showEditForm, setShowEditForm] = React.useState(false);

  const togglePostEdit = () => {
    setShowEditForm(!showEditForm);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        {!showEditForm ? (
          <Card.Description>{body}</Card.Description>
        ) : ''}
      </Card.Content>
      {showEditForm ? (
        <EditPost
          post={post}
          uploadImage={uploadImage}
          setShowEditForm={setShowEditForm}
          onEditSave={handlePostEdit}
          showEditForm={showEditForm}
        />
      ) : (
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostLike}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostDislike}
          >
            <Icon name={IconName.THUMBS_DOWN} />
            {dislikeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handleExpandedPostToggle}
          >
            <Icon name={IconName.COMMENT} />
            {commentCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => sharePost(id)}
          >
            <Icon name={IconName.SHARE_ALTERNATE} />
          </Label>
          {userId === user.id ? (
            <>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={togglePostEdit}
              >
                <Icon name={IconName.EDIT} />
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handlePostDelete}
              >
                <Icon name={IconName.DELETE} />
              </Label>
            </>
          ) : ('')}
        </Card.Content>
      )}
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  handlePostEdit: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired
};

export default Post;

import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import styles from './styles.module.scss';
import { IconName } from '../../../../common/enums/components/icon-name.enum';
import EditComment from '../edit-comment/edit-comment';

const Comment = ({ comment, onCommentLike, onCommentDislike, handleCommentEdit }) => {
  const { body, createdAt, user, id, likeCount, dislikeCount } = comment;
  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);

  const [showEditForm, setShowEditForm] = React.useState(false);

  const toggleCommentEdit = () => {
    setShowEditForm(!showEditForm);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        {showEditForm ? (
          <EditComment
            comment={comment}
            onEditCommentSave={handleCommentEdit}
            setShowEditForm={setShowEditForm}
            showEditForm={showEditForm}
          />
        ) : (
          <CommentUI.Content>
            <CommentUI.Text>{body}</CommentUI.Text>
            <CommentUI.Actions>
              <CommentUI.Action onClick={handleCommentLike}>
                <Icon name={IconName.THUMBS_UP} />
                {likeCount}
              </CommentUI.Action>
              <CommentUI.Action onClick={handleCommentDislike}>
                <Icon name={IconName.THUMBS_DOWN} />
                {dislikeCount}
              </CommentUI.Action>
              {userId === user.id ? (
                <CommentUI.Action onClick={toggleCommentEdit}>
                  <Icon name={IconName.EDIT} />
                </CommentUI.Action>
              ) : ''}
            </CommentUI.Actions>
          </CommentUI.Content>
        )}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  handleCommentEdit: PropTypes.func.isRequired
};

export default Comment;

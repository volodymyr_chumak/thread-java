import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Card, Image, Form, Button } from 'src/components/common/common';

import { postType } from 'src/common/prop-types/prop-types';
import styles from './styles.module.scss';

const EditPost = ({ post, showEditForm, setShowEditForm, uploadImage, onEditSave }) => {
  const togglePostEdit = () => {
    setShowEditForm(!showEditForm);
  };

  const [body, setBody] = React.useState(post.body);
  const postImage = post.image;
  const initImageState = postImage ? { id: postImage.id, link: postImage.link } : undefined;
  const [image, setImage] = React.useState(initImageState);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleEditedPostSave = async () => {
    await onEditSave({ id: post.id, imageId: image?.id, body });
    setBody('');
    setImage(undefined);
    togglePostEdit();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Card style={{ width: '100%' }}>
      <Card.Content>
        <Form onSubmit={handleEditedPostSave}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="Write"
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.link && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.link} alt="post" />
            </div>
          )}
          <div className={styles.btnWrapper}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
            <Button
              onClick={togglePostEdit}
              color="olive"
              iconName={IconName.CANCEL}
            >
              <label className={styles.btnImgLabel}>
                Cancel
              </label>
            </Button>
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Update
            </Button>
          </div>
        </Form>
      </Card.Content>
    </Card>
  );
};

EditPost.propTypes = {
  post: postType.isRequired,
  showEditForm: PropTypes.bool.isRequired,
  setShowEditForm: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onEditSave: PropTypes.func.isRequired
};

export default EditPost;

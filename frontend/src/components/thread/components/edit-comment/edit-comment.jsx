import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Form, Button } from 'src/components/common/common';

import { commentType } from 'src/common/prop-types/prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import styles from './styles.module.scss';

const EditComment = ({ comment, showEditForm, setShowEditForm, onEditCommentSave }) => {
  const toggleCommentEdit = () => {
    setShowEditForm(!showEditForm);
  };

  const [body, setBody] = React.useState(comment.body);

  const handleEditedCommentSave = async () => {
    await onEditCommentSave({ id: comment.id, body });
    setBody('');
    toggleCommentEdit();
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Content>
        <Form onSubmit={handleEditedCommentSave}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="Write"
            onChange={ev => setBody(ev.target.value)}
          />
          <div className={styles.btnWrapper}>
            <Button
              onClick={toggleCommentEdit}
              color="olive"
              iconName={IconName.CANCEL}
            >
              <label className={styles.btnImgLabel}>
                Cancel
              </label>
            </Button>
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Update
            </Button>
          </div>
        </Form>
      </CommentUI.Content>
    </CommentUI>
  );
};

EditComment.propTypes = {
  comment: commentType.isRequired,
  showEditForm: PropTypes.bool.isRequired,
  setShowEditForm: PropTypes.func.isRequired,
  onEditCommentSave: PropTypes.func.isRequired
};

export default EditComment;

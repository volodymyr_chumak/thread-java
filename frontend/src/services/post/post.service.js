import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Post {
  constructor({ http }) {
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load('/api/posts', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load('/api/posts', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updatePost(payload) {
    return this._http.load('/api/posts/update', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deletePost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.DELETE,
      contentType: ContentType.JSON
    });
  }

  likePost(postId) {
    return this.makeReaction(postId, true);
  }

  dislikePost(postId) {
    return this.makeReaction(postId, false);
  }

  makeReaction(postId, isReactionLike) {
    return this._http.load('/api/postreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: isReactionLike
      })
    });
  }
}

export { Post };

import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(payload) {
    return this._http.load('/api/comments/update', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId) {
    return this.makeReaction(commentId, true);
  }

  dislikeComment(commentId) {
    return this.makeReaction(commentId, false);
  }

  makeReaction(commentId, isReactionLike) {
    return this._http.load('/api/commentreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: isReactionLike
      })
    });
  }
}

export { Comment };

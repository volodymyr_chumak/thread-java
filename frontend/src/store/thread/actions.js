import { createAction } from '@reduxjs/toolkit';
import { comment as commentService, post as postService } from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  DELETE_POST: 'thread/delete-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const deletePost = createAction(ActionType.DELETE_POST, id => ({
  payload: {
    id
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePosts = (dispatch, getRootState, postId, map) => {
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : map(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(map(expandedPost)));
  }
};

const deletePostById = id => async dispatch => {
  const response = await postService.deletePost(id);

  dispatch(deletePost(response.id));
};

const saveEditedPost = post => async (dispatch, getRootState) => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);

  updatePosts(dispatch, getRootState, id, () => updatedPost);
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const makeReaction = (dispatch, getRootState, postId, diffLikes, diffDislikes) => {
  const map = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diffDislikes, // diff is taken from the current closure
    likeCount: Number(post.likeCount) + diffLikes
  });

  updatePosts(dispatch, getRootState, postId, map);
};

const getDiff = response => {
  const reactionStatus = response ? response.status : '';
  return reactionStatus === 'SWAP' ? -1 : 0;
};

const likePost = postId => async (dispatch, getRootState) => {
  const response = await postService.likePost(postId);
  const diffLikes = response?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislikes = getDiff(response);

  makeReaction(dispatch, getRootState, postId, diffLikes, diffDislikes);
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const response = await postService.dislikePost(postId);
  const diffDislikes = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise-dislike was removed
  const diffLikes = getDiff(response);

  makeReaction(dispatch, getRootState, postId, diffLikes, diffDislikes);
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  updatePosts(dispatch, getRootState, comment.postId, mapComments);
};

const saveEditedComment = comment => async (dispatch, getRootState) => {
  const { id, postId } = await commentService.updateComment(comment);
  const updatedComment = await commentService.getComment(id);

  const updatedComments = post => {
    if (!post.comments) return [];
    if (post.id !== comment.postId) return post.comments;

    return post.comments.map(c => (c.id === comment.id ? updatedComment : c));
  };

  const mapComments = post => ({
    ...post,
    comments: updatedComments(post)
  });

  updatePosts(dispatch, getRootState, postId, mapComments);
};

const makeCommentReaction = (dispatch, getRootState, comment, diffLikes, diffDislikes) => {
  const updatedComments = post => {
    if (!post.comments) return [];
    if (post.id !== comment.postId) return post.comments;

    return post.comments.map(c => (c.id === comment.id ? {
      ...c,
      dislikeCount: Number(c.dislikeCount) + diffDislikes,
      likeCount: Number(c.likeCount) + diffLikes
    } : c));
  };

  const mapComments = post => ({
    ...post,
    comments: updatedComments(post)
  });

  updatePosts(dispatch, getRootState, comment.postId, mapComments);
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const response = await commentService.likeComment(commentId);
  const diffLikes = response?.id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  const diffDislikes = getDiff(response);

  makeCommentReaction(dispatch, getRootState, comment, diffLikes, diffDislikes);
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const response = await commentService.dislikeComment(commentId);
  const diffDislikes = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise-dislike was removed
  const diffLikes = getDiff(response);

  makeCommentReaction(dispatch, getRootState, comment, diffLikes, diffDislikes);
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  deletePost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  saveEditedPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  likeComment,
  dislikeComment,
  saveEditedComment,
  deletePostById
};

package com.threadjava.postReactions.dto;

import com.threadjava.common.enums.ReactionStatus;
import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ResponsePostReactionDto {
    private UUID id;
    private UUID postId;
    private Boolean isLike;
    private UUID userId;
    private UUID authorId;
    private ReactionStatus status;
}

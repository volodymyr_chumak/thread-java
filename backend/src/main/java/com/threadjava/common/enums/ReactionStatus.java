package com.threadjava.common.enums;

public enum ReactionStatus {
    ADD,
    DELETE,
    SWAP
}

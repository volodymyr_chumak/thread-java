package com.threadjava.comment;

import com.threadjava.comment.dto.CommentListQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.awt.print.Pageable;
import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    List<Comment> findAllByPostId(UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentListQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.user) " +
            "FROM Comment c " +
            "WHERE (cast(:postId as string) is null OR c.post.id = :postId) " +
            "order by c.createdAt desc")
    List<CommentListQueryResult> findAllComments(@Param("postId") UUID postId);
}
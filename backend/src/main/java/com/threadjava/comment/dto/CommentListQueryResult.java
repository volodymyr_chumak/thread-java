package com.threadjava.comment.dto;

import com.threadjava.users.model.User;
import lombok.*;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentListQueryResult {
    public UUID id;
    public String body;
    public long likeCount;
    public long dislikeCount;
    public Date createdAt;
    public User user;
}

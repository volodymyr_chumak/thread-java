package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.post.dto.PostUpdateDto;
import com.threadjava.post.dto.PostUpdateResponseDto;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public CommentDetailsDto update(CommentUpdateDto commentDto) {
        CommentDetailsDto commentDetails = getCommentById(commentDto.getId());
        commentDto.setPostId(commentDetails.getPostId());
        Comment updatedComment = CommentMapper.MAPPER.commentDetailsDtoToComment(commentDto);
        updatedComment.setCreatedAt(commentDetails.getCreatedAt());
        Comment commentUpdated = commentRepository.save(updatedComment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(commentUpdated);
    }
}

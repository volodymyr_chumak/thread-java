package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.commentReactions.dto.RecievedCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody RecievedCommentReactionDto commentReaction){
        commentReaction.setUserId(getUserId());
        var reaction = commentsService.setReaction(commentReaction);

        if (reaction.isPresent() && !getUserId().equals(reaction.get().getAuthorId())) {
            // notify a user if someone (not himself) liked his comment
            template.convertAndSendToUser(
                    reaction.get().getAuthorId().toString(),
                    "/like",
                    "Your comment was liked!"
            );
        }
        return reaction;
    }
}

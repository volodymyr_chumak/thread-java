package com.threadjava.commentReactions.model;

import com.threadjava.comment.model.Comment;
import com.threadjava.db.BaseEntity;
import com.threadjava.users.model.User;
import lombok.*;
import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "comment_reactions")
public class CommentReaction extends BaseEntity {

    @Column(name = "isLike")
    private Boolean isLike;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "comment_id")
    private Comment comment;
}

package com.threadjava.commentReactions.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class RecievedCommentReactionDto {
    private UUID commentId;
    private UUID userId;
    private Boolean isLike;
}

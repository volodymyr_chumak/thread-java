package com.threadjava.commentReactions.dto;

import com.threadjava.common.enums.ReactionStatus;

import com.threadjava.common.enums.ReactionStatus;
import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ResponseCommentReactionDto {
    private UUID id;
    private UUID commentId;
    private Boolean isLike;
    private UUID userId;
    private UUID authorId;
    private ReactionStatus status;
}

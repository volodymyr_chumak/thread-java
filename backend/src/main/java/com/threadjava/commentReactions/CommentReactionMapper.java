package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.RecievedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.commentReactions.model.CommentReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentReactionMapper {
    com.threadjava.commentReactions.CommentReactionMapper MAPPER = Mappers.getMapper( com.threadjava.commentReactions.CommentReactionMapper.class );

    @Mapping(source = "comment.id", target = "commentId")
    @Mapping(source = "user.id", target = "userId")
    ResponseCommentReactionDto reactionToCommentReactionDto(CommentReaction commentReaction);

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "commentId", target = "comment.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    CommentReaction dtoToCommentReaction(RecievedCommentReactionDto commentReactionDto);
}

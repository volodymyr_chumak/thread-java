package com.threadjava.commentReactions;

import com.threadjava.common.enums.ReactionStatus;
import com.threadjava.comment.CommentService;
import com.threadjava.commentReactions.dto.RecievedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    @Autowired
    private CommentService commentsService;

    public Optional<ResponseCommentReactionDto> setReaction(RecievedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionsRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                commentReactionsRepository.deleteById(react.getId());
                var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
                var result = commentReactionsRepository.save(commentReaction);
                var comment = commentsService.getCommentById(result.getComment().getId());
                return Optional.of(
                        ResponseCommentReactionDto
                                .builder()
                                .id(result.getId())
                                .isLike(result.getIsLike())
                                .userId(result.getUser().getId())
                                .commentId(comment.getId())
                                .authorId(comment.getUser().getId())
                                .status(ReactionStatus.SWAP)
                                .build()
                );
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionsRepository.save(commentReaction);
            var comment = commentsService.getCommentById(result.getComment().getId());
            return Optional.of(
                    ResponseCommentReactionDto
                            .builder()
                            .id(result.getId())
                            .isLike(result.getIsLike())
                            .userId(result.getUser().getId())
                            .commentId(comment.getId())
                            .authorId(comment.getUser().getId())
                            .status(ReactionStatus.ADD)
                            .build()
            );
        }
    }
}

